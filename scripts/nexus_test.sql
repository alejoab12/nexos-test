-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-11-2020 a las 18:09:21
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nexus_test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit`
--

CREATE TABLE `audit` (
  `id` varchar(255) NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `parameters` text,
  `response` text,
  `user_agent` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit_error`
--

CREATE TABLE `audit_error` (
  `id` varchar(255) NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `trace` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `audit_error`
--

INSERT INTO `audit_error` (`id`, `creation_date`, `message`, `trace`) VALUES
('a15b4480-4f4c-4d2f-9517-a992865e4116', '2020-11-08 16:44:05', 'Hace falta varios parametros', 'Hace falta varios parametros'),
('ab6e4339-b3e3-4e42-b56f-706a4df59a02', '2020-11-08 16:44:05', 'Hace falta varios parametros', 'Hace falta varios parametros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `document_type`
--

CREATE TABLE `document_type` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `document_type`
--

INSERT INTO `document_type` (`id`, `name`) VALUES
('ad6e5a43-1396-4303-814d-9a74f8d753d6', 'Cedula'),
('ad6e5a43-1733-2335-814d-9a52f8d753d6', 'Pasaporte'),
('ad6khf43-1391-4305-814d-9a52f8d753d6', 'TI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `family`
--

CREATE TABLE `family` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `family`
--

INSERT INTO `family` (`id`, `name`) VALUES
('ad9e1a42-1397-3305-263d-7a52f1d752d0', 'Alcala Bustos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `member_type`
--

CREATE TABLE `member_type` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `member_type`
--

INSERT INTO `member_type` (`id`, `name`) VALUES
('ad6e5a43-1391-4205-816d-9a52f8d7a4u6', 'Padre'),
('ad6e5a43-2391-1108-874d-2a52f8d753d6', 'Madre'),
('ad9e1a42-1397-4305-863d-9a52f8d753d6', 'Hijo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE `person` (
  `id` varchar(255) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `document` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `document_type_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `person`
--

INSERT INTO `person` (`id`, `birthdate`, `document`, `email`, `last_name`, `name`, `phone`, `document_type_id`) VALUES
('ad6e5243-1394-4105-816d-9a12f1d7a4u6', '2015-02-08', '1010809234', 'martin20@hotmail.com', 'Alcala Bustos', 'Martin Alejandro', '3156566551', 'ad6khf43-1391-4305-814d-9a52f8d753d6'),
('ad6e5a43-1391-4205-816d-9a52f8d7a4u6', '1990-07-16', '1010194766', 'alejoab12@hotmail.com', 'Alcala Bustos', 'Manuel Alejandro', '3156566551', 'ad6e5a43-1396-4303-814d-9a74f8d753d6');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person_family`
--

CREATE TABLE `person_family` (
  `id` varchar(255) NOT NULL,
  `family_id` varchar(255) DEFAULT NULL,
  `member_type_id` varchar(255) DEFAULT NULL,
  `person_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `person_family`
--

INSERT INTO `person_family` (`id`, `family_id`, `member_type_id`, `person_id`) VALUES
('ae0e1a42-1397-3305-263d-2a12f1d652d0', 'ad9e1a42-1397-3305-263d-7a52f1d752d0', 'ad6e5a43-1391-4205-816d-9a52f8d7a4u6', 'ad6e5a43-1391-4205-816d-9a52f8d7a4u6'),
('ae0e1a42-1397-3305-263d-2a12f1d653d1', 'ad9e1a42-1397-3305-263d-7a52f1d752d0', 'ad9e1a42-1397-4305-863d-9a52f8d753d6', 'ad6e5243-1394-4105-816d-9a12f1d7a4u6');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `audit`
--
ALTER TABLE `audit`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audit_error`
--
ALTER TABLE `audit_error`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `document_type`
--
ALTER TABLE `document_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `family`
--
ALTER TABLE `family`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `member_type`
--
ALTER TABLE `member_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_fwmwi44u55bo4rvwsv0cln012` (`email`),
  ADD KEY `FKjub676u03pe62eth6ps1dvdty` (`document_type_id`);

--
-- Indices de la tabla `person_family`
--
ALTER TABLE `person_family`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK64xcli6p72lrpebijvlo5s3gw` (`family_id`),
  ADD KEY `FKdd417sogvftxdk6jqmqxykco7` (`member_type_id`),
  ADD KEY `FKmgcjiqdmlhsh17ewn8vgs30a7` (`person_id`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `person`
--
ALTER TABLE `person`
  ADD CONSTRAINT `FKjub676u03pe62eth6ps1dvdty` FOREIGN KEY (`document_type_id`) REFERENCES `document_type` (`id`);

--
-- Filtros para la tabla `person_family`
--
ALTER TABLE `person_family`
  ADD CONSTRAINT `FK64xcli6p72lrpebijvlo5s3gw` FOREIGN KEY (`family_id`) REFERENCES `family` (`id`),
  ADD CONSTRAINT `FKdd417sogvftxdk6jqmqxykco7` FOREIGN KEY (`member_type_id`) REFERENCES `member_type` (`id`),
  ADD CONSTRAINT `FKmgcjiqdmlhsh17ewn8vgs30a7` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
