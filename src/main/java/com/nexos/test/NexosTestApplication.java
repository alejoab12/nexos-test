package com.nexos.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
/**
 * @author Malejoab1990
 */
@SpringBootApplication
@EnableAsync
public class NexosTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(NexosTestApplication.class, args);
    }

}
