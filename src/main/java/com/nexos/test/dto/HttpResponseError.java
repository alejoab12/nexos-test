package com.nexos.test.dto;

import lombok.Data;
import lombok.experimental.SuperBuilder;
/**
 * @author Malejoab1990
 */
@Data
@SuperBuilder
public class HttpResponseError {
    private String message;
    private String status;
}
