package com.nexos.test.dto;

import lombok.Data;
/**
 * @author Malejoab1990
 */
@Data
public class MemberTypeDTO {
    private String id;
    private String name;
}
