package com.nexos.test.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
/**
 * @author Malejoab1990
 */
@Data
public class FamilyDTO {
    private String id;
    @NotBlank
    private String name;
}
