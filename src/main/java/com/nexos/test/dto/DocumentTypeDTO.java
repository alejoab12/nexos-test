package com.nexos.test.dto;

import com.nexos.test.constant.Constants;
import lombok.Data;

import javax.validation.constraints.NotBlank;
/**
 * @author Malejoab1990
 */
@Data
public class DocumentTypeDTO {
    private String id;
    @NotBlank(message = Constants.DOCUMENT_TYPE_NAME_NOT_EMPTY)
    private String name;
}
