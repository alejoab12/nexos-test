package com.nexos.test.dto;

import lombok.Data;

import java.util.Date;
/**
 * @author Malejoab1990
 */
@Data
public class PersonDTO {
    private String id;
    private String document;
    private DocumentTypeDTO documentType;
    private String name;
    private String lastName;
    private String email;
    private String phone;
    private Date birthdate;
}
