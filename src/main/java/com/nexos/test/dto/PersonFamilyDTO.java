package com.nexos.test.dto;

import lombok.Data;
/**
 * @author Malejoab1990
 */
@Data
public class PersonFamilyDTO {
    private String id;
    private PersonDTO person;
    private FamilyDTO family;
    private MemberTypeDTO memberType;
}
