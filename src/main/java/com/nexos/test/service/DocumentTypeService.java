package com.nexos.test.service;

import com.nexos.test.dto.DocumentTypeDTO;

import java.util.List;
/**
 * @author Malejoab1990
 */
public interface DocumentTypeService {
    //Permite consultar los diferentes tipos de documento
    List<DocumentTypeDTO> documentTypeAll();

    //Permite actualizar un tipo de documento
    void documentTypeUpdate(DocumentTypeDTO documentTypeDTO);

    //Permite eliminar un tipo de documento por id
    void documentTypeDelete(String id);

    //Permite crear un tipo de documento
    void documentTypeCreate(DocumentTypeDTO documentTypeDTO);
}
