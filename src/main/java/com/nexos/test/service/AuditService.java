package com.nexos.test.service;

import com.nexos.test.persistence.entity.AuditEntity;
import com.nexos.test.persistence.entity.AuditErrorEntity;

/**
 * @author Malejoab1990
 */
public interface AuditService {
    //Permite registrar auditoría generada por los servicios
    void auditCreate(AuditEntity auditEntity);

    void auditErrorCreate(AuditErrorEntity auditErrorEntity);
}
