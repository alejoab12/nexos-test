package com.nexos.test.service;

import com.nexos.test.dto.PersonFamilyDTO;

import java.util.List;
/**
 * @author Malejoab1990
 */
public interface PersonFamilyService {
    //Permite consultar la asociacion entre persona y familia por id
    PersonFamilyDTO findById(String id);

    //Permite consultar los miembros de una familia, por el documento de alguno de sus integrantes
    List<PersonFamilyDTO> findPersonFamilyByDocumentAndDocumentType(String document, String documentType);

    //Permite agregar una persona a una familia
    void addPersonToFamily(String familyId, String personId, String memberTypeId);

    //Permite remover una persona de una familia
    void removePersonFromFamily(String familyId, String personId);

}
