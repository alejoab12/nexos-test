package com.nexos.test.service;

import com.nexos.test.dto.PersonDTO;

/**
 * @author Malejoab1990
 */
public interface PersonService {

    //Permite consultar una persona por documento y tipo de documento
    PersonDTO personByDocument(String document, String documentType);

    //Permite consultar una persona por email
    PersonDTO personByEmail(String email);

    //Permite crear una persona
    void personCreate(PersonDTO person);

    //Permite actualizar una persona
    void personUpdate(PersonDTO person);

    //Permite eliminar una persona por su id
    void personDelete(String id);
}
