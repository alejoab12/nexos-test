package com.nexos.test.service.impl;

import com.nexos.test.audit.Audit;
import com.nexos.test.constant.Constants;
import com.nexos.test.dto.DocumentTypeDTO;
import com.nexos.test.exception.RestException;
import com.nexos.test.mapper.DocumentTypeMapper;
import com.nexos.test.persistence.entity.DocumentTypeEntity;
import com.nexos.test.persistence.repository.DocumentTypeRepository;
import com.nexos.test.service.DocumentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/**
 * @author Malejoab1990
 */
@Transactional
@Service
public class DocumentTypeServiceImpl implements DocumentTypeService {
    private final DocumentTypeMapper documentTypeMapper;
    private final DocumentTypeRepository documentTypeRepository;

    public DocumentTypeServiceImpl(@Autowired DocumentTypeRepository documentTypeRepository,
                                   @Autowired DocumentTypeMapper documentTypeMapper) {
        this.documentTypeMapper = documentTypeMapper;
        this.documentTypeRepository = documentTypeRepository;
    }

    @Audit
    @Override
    public List<DocumentTypeDTO> documentTypeAll() {
        return documentTypeRepository.findAll().stream().map(documentTypeMapper::toDTO).collect(Collectors.toList());
    }

    @Audit
    @Override
    public void documentTypeCreate(DocumentTypeDTO documentTypeDTO) {
        documentTypeRepository.saveAndFlush(documentTypeMapper.toEntity(documentTypeDTO));
    }

    @Audit
    @Override
    public void documentTypeUpdate(DocumentTypeDTO documentTypeDTO) {
        Optional<DocumentTypeEntity> documentTypeEntityOptional = documentTypeRepository.findById(documentTypeDTO.getId());
        if (!documentTypeEntityOptional.isPresent()) {
            throw new RestException(Constants.NOT_FOUND_DOCUMENT_TYPE, HttpStatus.NOT_FOUND);
        }
        documentTypeRepository.saveAndFlush(documentTypeMapper.toEntity(documentTypeDTO));
    }

    @Audit
    @Override
    public void documentTypeDelete(String id) {
        documentTypeRepository.delete(new DocumentTypeEntity(id));
    }


}
