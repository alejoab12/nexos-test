package com.nexos.test.service.impl;

import com.nexos.test.constant.Constants;
import com.nexos.test.dto.PersonDTO;
import com.nexos.test.exception.RestException;
import com.nexos.test.mapper.PersonMapper;
import com.nexos.test.persistence.entity.DocumentTypeEntity;
import com.nexos.test.persistence.entity.PersonEntity;
import com.nexos.test.persistence.repository.PersonRepository;
import com.nexos.test.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
/**
 * @author Malejoab1990
 */
@Transactional
@Service
public class PersonServiceImpl implements PersonService {
    private final PersonMapper personMapper;
    private final PersonRepository personRepository;

    public PersonServiceImpl(@Autowired PersonRepository personRepository,
                             @Autowired PersonMapper personMapper) {
        this.personMapper = personMapper;
        this.personRepository = personRepository;
    }

    @Override
    public PersonDTO personByDocument(String document, String documentType) {
        Optional<PersonEntity> personEntityOptional = this.personRepository.findByDocumentAndDocumentType(document, new DocumentTypeEntity(documentType));
        if (!personEntityOptional.isPresent()) {
            buildRestException();
        }
        return this.personMapper.toDTO(personEntityOptional.get());
    }

    @Override
    public PersonDTO personByEmail(String email) {
        Optional<PersonEntity> personEntityOptional = this.personRepository.findByEmail(email);
        if (!personEntityOptional.isPresent()) {
            buildRestException();
        }
        return this.personMapper.toDTO(personEntityOptional.get());
    }

    @Override
    public void personCreate(PersonDTO personDTO) {
        Optional<PersonEntity> personEntityOptional = this.personRepository.findByDocumentAndDocumentTypeOrEmail(personDTO.getDocument(),
                new DocumentTypeEntity(personDTO.getDocumentType().getId()),
                personDTO.getEmail());
        if (personEntityOptional.isPresent()) {
            throw new RestException(Constants.EXIST_PERSON, HttpStatus.BAD_REQUEST);
        }
        this.personRepository.saveAndFlush(this.personMapper.toEntity(personDTO));
    }

    @Override
    public void personUpdate(PersonDTO personDTO) {
        if (!personRepository.existsById(personDTO.getId())) {
            buildRestException();
        }
        this.personRepository.saveAndFlush(this.personMapper.toEntity(personDTO));
    }

    @Override
    public void personDelete(String id) {
        if (!personRepository.existsById(id)) {
            buildRestException();
        }
        this.personRepository.deleteById(id);
    }


    private void buildRestException() {
        throw new RestException(Constants.NOT_FOUND_PERSON, HttpStatus.NOT_FOUND);
    }
}
