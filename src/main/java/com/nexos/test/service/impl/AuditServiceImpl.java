package com.nexos.test.service.impl;

import com.nexos.test.persistence.entity.AuditEntity;
import com.nexos.test.persistence.entity.AuditErrorEntity;
import com.nexos.test.persistence.repository.AuditErrorRepository;
import com.nexos.test.persistence.repository.AuditRepository;
import com.nexos.test.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
/**
 * @author Malejoab1990
 */
@Service
public class AuditServiceImpl implements AuditService {
    private final AuditRepository auditRepository;
    private final AuditErrorRepository auditErrorRepository;

    public AuditServiceImpl(@Autowired AuditRepository auditRepository, @Autowired AuditErrorRepository auditErrorRepository) {
        this.auditRepository = auditRepository;
        this.auditErrorRepository = auditErrorRepository;
    }

    @Async
    @Override
    public void auditCreate(AuditEntity auditEntity) {
        this.auditRepository.save(auditEntity);
    }

    @Async
    @Override
    public void auditErrorCreate(AuditErrorEntity auditErrorEntity) {
        this.auditErrorRepository.save(auditErrorEntity);
    }
}
