package com.nexos.test.service.impl;

import com.nexos.test.audit.Audit;
import com.nexos.test.constant.Constants;
import com.nexos.test.dto.MemberTypeDTO;
import com.nexos.test.exception.RestException;
import com.nexos.test.mapper.MemberTypeMapper;
import com.nexos.test.persistence.entity.MemberTypeEntity;
import com.nexos.test.persistence.repository.MemberTypeRepository;
import com.nexos.test.service.MemberTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/**
 * @author Malejoab1990
 */
@Service
@Transactional
public class MemberTypeServiceImpl implements MemberTypeService {
    private final MemberTypeRepository memberTypeRepository;
    private final MemberTypeMapper memberTypeMapper;

    public MemberTypeServiceImpl(@Autowired MemberTypeRepository memberTypeRepository, @Autowired MemberTypeMapper memberTypeMapper) {
        this.memberTypeMapper = memberTypeMapper;
        this.memberTypeRepository = memberTypeRepository;
    }

    @Audit
    @Override
    public List<MemberTypeDTO> memberTypeAll() {
        return this.memberTypeRepository.findAll().stream().map(memberTypeMapper::toDTO).collect(Collectors.toList());
    }

    @Audit
    @Override
    public void memberTypeUpdate(MemberTypeDTO memberTypeDTO) {
        Optional<MemberTypeEntity> memberTypeEntityOptional = this.memberTypeRepository.findById(memberTypeDTO.getId());
        if (!memberTypeEntityOptional.isPresent()) {
            buildRestException();
        }
        this.memberTypeRepository.saveAndFlush(memberTypeMapper.toEntity(memberTypeDTO));
    }

    @Audit
    @Override
    public void memberTypeDelete(String id) {
        Optional<MemberTypeEntity> memberTypeEntityOptional = this.memberTypeRepository.findById(id);
        if (!memberTypeEntityOptional.isPresent()) {
            buildRestException();
        }
        this.memberTypeRepository.deleteById(id);
    }

    @Audit
    @Override
    public void memberTypeCreate(MemberTypeDTO memberTypeDTO) {
        this.memberTypeRepository.saveAndFlush(this.memberTypeMapper.toEntity(memberTypeDTO));
    }

    private void buildRestException() {
        throw new RestException(Constants.NOT_FOUND_MEMBER_TYPE, HttpStatus.NOT_FOUND);
    }
}
