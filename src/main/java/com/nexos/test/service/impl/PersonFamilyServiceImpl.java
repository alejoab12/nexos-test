package com.nexos.test.service.impl;

import com.nexos.test.audit.Audit;
import com.nexos.test.constant.Constants;
import com.nexos.test.dto.PersonFamilyDTO;
import com.nexos.test.exception.RestException;
import com.nexos.test.mapper.PersonFamilyMapper;
import com.nexos.test.persistence.entity.*;
import com.nexos.test.persistence.repository.FamilyRepository;
import com.nexos.test.persistence.repository.PersonFamilyRepository;
import com.nexos.test.persistence.repository.PersonRepository;
import com.nexos.test.service.PersonFamilyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/**
 * @author Malejoab1990
 */
@Transactional
@Service
public class PersonFamilyServiceImpl implements PersonFamilyService {
    private final PersonFamilyRepository personFamilyRepository;
    private final PersonRepository personRepository;
    private final FamilyRepository familyRepository;
    private final PersonFamilyMapper personFamilyMapper;

    public PersonFamilyServiceImpl(@Autowired PersonFamilyRepository personFamilyRepository,
                                   @Autowired PersonFamilyMapper personFamilyMapper,
                                   @Autowired PersonRepository personRepository,
                                   @Autowired FamilyRepository familyRepository
    ) {
        this.personFamilyRepository = personFamilyRepository;
        this.personFamilyMapper = personFamilyMapper;
        this.personRepository = personRepository;
        this.familyRepository = familyRepository;
    }

    @Audit
    @Override
    public PersonFamilyDTO findById(String id) {
        Optional<PersonFamilyEntity> personFamilyEntityOptional = this.personFamilyRepository.findById(id);
        if (!personFamilyEntityOptional.isPresent()) {
            buildRestException();
        }
        return this.personFamilyMapper.toDTO(personFamilyEntityOptional.get());
    }

    @Audit
    @Override
    public List<PersonFamilyDTO> findPersonFamilyByDocumentAndDocumentType(String document, String documentTypeId) {
        Optional<PersonEntity> personEntityOptional = this.personRepository.findByDocumentAndDocumentType(document, new DocumentTypeEntity(documentTypeId));
        if (!personEntityOptional.isPresent()) {
            buildRestException();
        }
        Optional<PersonFamilyEntity> personFamilyEntityOptional = this.personFamilyRepository.findByPersonId(personEntityOptional.get().getId());
        if (!personFamilyEntityOptional.isPresent()) {
            buildRestException();
        }
        List<PersonFamilyEntity> membersFamily = this.personFamilyRepository.findByFamilyId(personFamilyEntityOptional.get().getFamily().getId());
        return membersFamily.stream().map(this.personFamilyMapper::toDTO).collect(Collectors.toList());
    }

    @Audit
    @Override
    public void addPersonToFamily(String familyId, String personId, String memberTypeId) {
        if (!this.personRepository.existsById(personId) || !this.familyRepository.existsById(familyId)) {
            throw new RestException(Constants.NOT_FOUND, HttpStatus.NOT_FOUND);
        }
        this.personFamilyRepository.saveAndFlush(new PersonFamilyEntity(
                new PersonEntity(personId),
                new FamilyEntity(familyId),
                new MemberTypeEntity(memberTypeId)));
    }

    @Audit
    @Override
    public void removePersonFromFamily(String familyId, String personId) {
        Optional<PersonFamilyEntity> personFamilyEntityOptional = this.personFamilyRepository.findByPersonIdAndFamilyId(personId, familyId);
        if (!personFamilyEntityOptional.isPresent()) {
            buildRestException();
        }
        this.personFamilyRepository.delete(personFamilyEntityOptional.get());
    }

    private void buildRestException() {
        throw new RestException(Constants.NOT_FOUND_PERSON_FAMILY, HttpStatus.NOT_FOUND);
    }
}
