package com.nexos.test.service.impl;

import com.nexos.test.audit.Audit;
import com.nexos.test.constant.Constants;
import com.nexos.test.dto.FamilyDTO;
import com.nexos.test.exception.RestException;
import com.nexos.test.mapper.FamilyMapper;
import com.nexos.test.persistence.entity.FamilyEntity;
import com.nexos.test.persistence.repository.FamilyRepository;
import com.nexos.test.service.FamilyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/**
 * @author Malejoab1990
 */
@Transactional
@Service
public class FamilyServiceImpl implements FamilyService {
    private final FamilyMapper familyMapper;
    private final FamilyRepository familyRepository;

    public FamilyServiceImpl(@Autowired FamilyRepository familyRepository, @Autowired FamilyMapper familyMapper) {
        this.familyMapper = familyMapper;
        this.familyRepository = familyRepository;
    }

    @Audit
    @Override
    public FamilyDTO findById(String id) {
        Optional<FamilyEntity> familyEntityOptional = this.familyRepository.findById(id);
        if (!familyEntityOptional.isPresent()) {
            buildRestException();
        }
        return this.familyMapper.toDTO(familyEntityOptional.get());

    }

    @Audit
    @Override
    public List<FamilyDTO> findByName(String name) {
        Optional<List<FamilyEntity>> familyEntityOptional = this.familyRepository.findByNameLike("%".concat(name).concat("%"));
        if (!familyEntityOptional.isPresent()) {
            buildRestException();
        }
        return familyEntityOptional.get().stream().map(this.familyMapper::toDTO).collect(Collectors.toList());
    }

    @Audit
    @Override
    public void familyUpdate(FamilyDTO familyDTO) {
        if (!this.familyRepository.existsById(familyDTO.getId())) {
            buildRestException();
        }
        this.familyRepository.saveAndFlush(this.familyMapper.toEntity(familyDTO));
    }

    @Audit
    @Override
    public void familyDelete(String id) {
        if (!this.familyRepository.existsById(id)) {
            buildRestException();
        }
        this.familyRepository.deleteById(id);
    }

    @Audit
    @Override
    public void familyCreate(FamilyDTO familyDTO) {
        this.familyRepository.saveAndFlush(this.familyMapper.toEntity(familyDTO));
    }

    private void buildRestException() {
        throw new RestException(Constants.NOT_FOUND_FAMILY, HttpStatus.NOT_FOUND);
    }
}
