package com.nexos.test.service;

import com.nexos.test.dto.FamilyDTO;

import java.util.List;
/**
 * @author Malejoab1990
 */
public interface FamilyService {
    //Permite buscar la familia por id
    FamilyDTO findById(String id);

    //Permite buscar la familia haciendo un like por nombre
    List<FamilyDTO> findByName(String name);

    //Permite actualizar la familia
    void familyUpdate(FamilyDTO familyDTO);

    //Permite eliminar una familia por id
    void familyDelete(String id);

    //Permite crear una familia
    void familyCreate(FamilyDTO familyDTO);
}
