package com.nexos.test.service;

import com.nexos.test.dto.MemberTypeDTO;

import java.util.List;
/**
 * @author Malejoab1990
 */
public interface MemberTypeService {
    //Permite consultar los diferentes tipos de miembros de una familia
    List<MemberTypeDTO> memberTypeAll();

    //Permite actualizar un tipo de miembro de una familia
    void memberTypeUpdate(MemberTypeDTO memberTypeDTO);

    //Permite eliminar un tipo de miembro por id
    void memberTypeDelete(String id);

    //Permite crear un tipo de miembro de una familia
    void memberTypeCreate(MemberTypeDTO memberTypeDTO);
}
