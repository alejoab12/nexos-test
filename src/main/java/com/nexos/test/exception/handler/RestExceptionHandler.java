package com.nexos.test.exception.handler;

import com.nexos.test.dto.HttpResponseError;
import com.nexos.test.exception.RestException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
/**
 * @author Malejoab1990
 */
@ControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler(value
            = {RestException.class})
    protected ResponseEntity<HttpResponseError> handleConflict(
            RestException ex, WebRequest request) {
        return ResponseEntity
                .status(ex.getStatus())
                .body(HttpResponseError
                        .builder()
                        .message(ex.getMessage())
                        .status(ex.getStatus()
                                .name()).build());
    }
}
