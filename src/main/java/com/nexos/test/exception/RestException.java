package com.nexos.test.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;
/**
 * @author Malejoab1990
 */
@Data
public class RestException extends RuntimeException {
    private String message;
    private HttpStatus status;

    public RestException(String message, HttpStatus status) {
        super(message);
        this.message = message;
        this.status = status;
    }
}
