package com.nexos.test.persistence.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * @author Malejoab1990
 */
@Data
@Table(name = "document_type")
@Entity
public class DocumentTypeEntity {
    @Id
    private String id;
    private String name;

    public DocumentTypeEntity(String id) {
        this.id = id;
    }

    public DocumentTypeEntity() {
    }
}
