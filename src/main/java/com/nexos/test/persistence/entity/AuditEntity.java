package com.nexos.test.persistence.entity;

import lombok.Data;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
/**
 * @author Malejoab1990
 */
@Data
@SuperBuilder
@Entity
@Table(name = "audit")
public class AuditEntity {
    @Id
    private String id;
    private String method;
    @Column(columnDefinition = "text")
    private String parameters;
    @Column(columnDefinition = "text")
    private String response;
    private String ip;
    private String userAgent;
    private Timestamp creationDate;

    public AuditEntity() {
    }
}
