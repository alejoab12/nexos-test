package com.nexos.test.persistence.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * @author Malejoab1990
 */
@Data
@Table(name = "person_family")
@Entity
public class PersonFamilyEntity {
    @Id
    private String id;
    @ManyToOne
    private PersonEntity person;
    @ManyToOne
    private FamilyEntity family;
    @ManyToOne
    private MemberTypeEntity memberType;

    public PersonFamilyEntity(PersonEntity person, FamilyEntity family, MemberTypeEntity memberType) {
        this.person = person;
        this.family = family;
        this.memberType = memberType;
    }

    public PersonFamilyEntity() {

    }
}
