package com.nexos.test.persistence.entity;

import lombok.Data;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
/**
 * @author Malejoab1990
 */
@Data
@SuperBuilder
@Entity
@Table(name = "audit_error")
public class AuditErrorEntity {
    @Id
    private String id;
    private String message;
    private Timestamp creationDate;

    public AuditErrorEntity() {
    }
}
