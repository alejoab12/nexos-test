package com.nexos.test.persistence.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * @author Malejoab1990
 */
@Data
@Table(name = "family")
@Entity
public class FamilyEntity {
    @Id
    private String id;
    private String name;

    public FamilyEntity(String id) {
        this.id = id;
    }

    public FamilyEntity() {

    }
}
