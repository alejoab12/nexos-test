package com.nexos.test.persistence.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * @author Malejoab1990
 */
@Data
@Table(name = "Member_type")
@Entity
public class MemberTypeEntity {
    @Id
    private String id;
    private String name;

    public MemberTypeEntity(String id) {
        this.id = id;
    }

    public MemberTypeEntity() {

    }
}
