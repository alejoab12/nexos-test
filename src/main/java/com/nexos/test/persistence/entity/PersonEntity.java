package com.nexos.test.persistence.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
/**
 * @author Malejoab1990
 */
@Data
@Table(name = "person")
@Entity
public class PersonEntity {
    @Id
    private String id;
    private String document;
    @JoinColumn(name = "document_type_id")
    @ManyToOne(targetEntity = DocumentTypeEntity.class)
    private DocumentTypeEntity documentType;
    private String name;
    private String lastName;
    @Column(unique = true)
    private String email;
    private String phone;
    @Column(columnDefinition = "date")
    private Date birthdate;

    public PersonEntity(String id) {
        this.id = id;
    }

    public PersonEntity() {
    }

}
