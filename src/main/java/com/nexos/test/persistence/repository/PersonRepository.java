package com.nexos.test.persistence.repository;

import com.nexos.test.persistence.entity.DocumentTypeEntity;
import com.nexos.test.persistence.entity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
/**
 * @author Malejoab1990
 */
@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, String> {
    Optional<PersonEntity> findByDocumentAndDocumentType(String document, DocumentTypeEntity documentType);

    Optional<PersonEntity> findByDocumentAndDocumentTypeOrEmail(String document, DocumentTypeEntity documentType, String email);

    Optional<PersonEntity> findByEmail(String email);
}
