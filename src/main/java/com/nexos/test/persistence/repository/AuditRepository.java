package com.nexos.test.persistence.repository;

import com.nexos.test.persistence.entity.AuditEntity;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * @author Malejoab1990
 */
public interface AuditRepository extends JpaRepository<AuditEntity, String> {
}
