package com.nexos.test.persistence.repository;

import com.nexos.test.persistence.entity.AuditErrorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * @author Malejoab1990
 */
public interface AuditErrorRepository extends JpaRepository<AuditErrorEntity, String> {
}
