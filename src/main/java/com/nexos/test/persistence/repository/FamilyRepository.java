package com.nexos.test.persistence.repository;

import com.nexos.test.persistence.entity.FamilyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
/**
 * @author Malejoab1990
 */
@Repository
public interface FamilyRepository extends JpaRepository<FamilyEntity, String> {

    Optional<List<FamilyEntity>> findByNameLike(String name);
}
