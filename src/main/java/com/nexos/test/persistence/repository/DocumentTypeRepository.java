package com.nexos.test.persistence.repository;

import com.nexos.test.persistence.entity.DocumentTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * @author Malejoab1990
 */
@Repository
public interface DocumentTypeRepository extends JpaRepository<DocumentTypeEntity, String> {
}
