package com.nexos.test.persistence.repository;

import com.nexos.test.persistence.entity.MemberTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * @author Malejoab1990
 */
@Repository
public interface MemberTypeRepository extends JpaRepository<MemberTypeEntity, String> {
}
