package com.nexos.test.persistence.repository;

import com.nexos.test.persistence.entity.PersonFamilyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
/**
 * @author Malejoab1990
 */
@Repository
public interface PersonFamilyRepository extends JpaRepository<PersonFamilyEntity, String> {
    Optional<PersonFamilyEntity> findByPersonId(String personId);

    List<PersonFamilyEntity> findByFamilyId(String id);

    Optional<PersonFamilyEntity> findByPersonIdAndFamilyId(String personId, String familyId);
}
