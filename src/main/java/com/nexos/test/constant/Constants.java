package com.nexos.test.constant;
/**
 * @author Malejoab1990
 */
public interface Constants {
    String NOT_FOUND = "Not Found";
    String NOT_FOUND_DOCUMENT_TYPE = NOT_FOUND.concat(" DocumentType");
    String NOT_FOUND_PERSON_FAMILY = NOT_FOUND.concat(" PersonFamily");
    String NOT_FOUND_FAMILY = NOT_FOUND.concat(" Family");
    String DOCUMENT_TYPE_NAME_NOT_EMPTY = "The name of the document type cannot be empty";
    String NOT_FOUND_MEMBER_TYPE = NOT_FOUND.concat(" MemberTYpe");
    String NOT_FOUND_PERSON = NOT_FOUND.concat(" Person");
    String EXIST_PERSON = "There is already a user registered with the document or email";
    String USER_AGENT = "User-Agent";
}
