package com.nexos.test.mapper;

import com.nexos.test.dto.DocumentTypeDTO;
import com.nexos.test.persistence.entity.DocumentTypeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Objects;
import java.util.UUID;
/**
 * @author Malejoab1990
 */
@Mapper(componentModel = "spring")
public abstract class DocumentTypeMapper {

    public abstract DocumentTypeDTO toDTO(DocumentTypeEntity entity);

    @Mapping(target = "id", source = "id", qualifiedByName = "generateId")
    public abstract DocumentTypeEntity toEntity(DocumentTypeDTO dto);

    @Named("generateId")
    String generateId(String id) {
        return Objects.isNull(id) ? UUID.randomUUID().toString() : id;
    }
}
