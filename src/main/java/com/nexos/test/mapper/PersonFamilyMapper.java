package com.nexos.test.mapper;

import com.nexos.test.dto.PersonFamilyDTO;
import com.nexos.test.persistence.entity.PersonFamilyEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Objects;
import java.util.UUID;
/**
 * @author Malejoab1990
 */
@Mapper(componentModel = "spring")
public abstract class PersonFamilyMapper {
    public abstract PersonFamilyDTO toDTO(PersonFamilyEntity entity);

    @Mapping(target = "id", source = "id", qualifiedByName = "generateId")
    public abstract PersonFamilyEntity toEntity(PersonFamilyDTO dto);

    @Named("generateId")
    String generateId(String id) {
        return Objects.isNull(id) ? UUID.randomUUID().toString() : id;
    }
}
