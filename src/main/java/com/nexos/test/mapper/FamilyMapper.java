package com.nexos.test.mapper;

import com.nexos.test.dto.FamilyDTO;
import com.nexos.test.persistence.entity.FamilyEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Objects;
import java.util.UUID;
/**
 * @author Malejoab1990
 */
@Mapper(componentModel = "spring")
public abstract class FamilyMapper {
    public abstract FamilyDTO toDTO(FamilyEntity entity);

    @Mapping(target = "id", source = "id", qualifiedByName = "generateId")
    public abstract FamilyEntity toEntity(FamilyDTO dto);

    @Named("generateId")
    String generateId(String id) {
        return Objects.isNull(id) ? UUID.randomUUID().toString() : id;
    }
}
