package com.nexos.test.mapper;

import com.nexos.test.dto.MemberTypeDTO;
import com.nexos.test.persistence.entity.MemberTypeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Objects;
import java.util.UUID;
/**
 * @author Malejoab1990
 */
@Mapper(componentModel = "spring")
public abstract class MemberTypeMapper {
    public abstract MemberTypeDTO toDTO(MemberTypeEntity entity);

    @Mapping(target = "id", source = "id", qualifiedByName = "generateId")
    public abstract MemberTypeEntity toEntity(MemberTypeDTO dto);

    @Named("generateId")
    String generateId(String id) {
        return Objects.isNull(id) ? UUID.randomUUID().toString() : id;
    }
}
