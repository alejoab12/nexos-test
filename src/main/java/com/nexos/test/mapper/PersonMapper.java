package com.nexos.test.mapper;

import com.nexos.test.dto.PersonDTO;
import com.nexos.test.persistence.entity.PersonEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Objects;
import java.util.UUID;
/**
 * @author Malejoab1990
 */
@Mapper(componentModel = "spring")
public abstract class PersonMapper {

    public abstract PersonDTO toDTO(PersonEntity entity);

    @Mapping(target = "id", source = "id", qualifiedByName = "generateId")
    public abstract PersonEntity toEntity(PersonDTO dto);

    @Named("generateId")
    String generateId(String id) {
        return Objects.isNull(id) ? UUID.randomUUID().toString() : id;
    }
}
