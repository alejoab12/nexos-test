package com.nexos.test.controller;

import com.nexos.test.dto.PersonFamilyDTO;
import com.nexos.test.service.PersonFamilyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;
/**
 * @author Malejoab1990
 */
@RestController
@RequestMapping("/api/person-family")
@Validated
public class PersonFamilyController {
    private final PersonFamilyService personFamilyService;

    public PersonFamilyController(PersonFamilyService personFamilyService) {
        this.personFamilyService = personFamilyService;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonFamilyDTO> findPersonFamilyById(@PathVariable String id) {
        return ResponseEntity.ok(personFamilyService.findById(id));

    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PersonFamilyDTO>> findPersonFamilyByDocumentAndDocumentType(@NotBlank @RequestParam String document, @NotBlank @RequestParam String documentType) {
        return ResponseEntity.ok(personFamilyService.findPersonFamilyByDocumentAndDocumentType(document, documentType));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addPersonToFamily(@RequestParam @NotBlank String familyId,
                                                  @RequestParam @NotBlank String personId,
                                                  @RequestParam @NotBlank String memberTypeId) {
        this.personFamilyService.addPersonToFamily(familyId, personId, memberTypeId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping
    public ResponseEntity<Void> removePersonFromFamily(String familyId, String personId) {
        this.personFamilyService.removePersonFromFamily(familyId, personId);
        return ResponseEntity.ok().build();
    }
}