package com.nexos.test.controller;

import com.nexos.test.dto.MemberTypeDTO;
import com.nexos.test.service.MemberTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
/**
 * @author Malejoab1990
 */
@RestController
@RequestMapping("/api/member-type")
public class MemberTypeController {
    private final MemberTypeService memberTypeService;

    public MemberTypeController(@Autowired MemberTypeService memberTypeService) {
        this.memberTypeService = memberTypeService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MemberTypeDTO>> memberTypeAll() {
        return ResponseEntity.ok(this.memberTypeService.memberTypeAll());
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> memberTypeUpdate(@Valid @RequestBody MemberTypeDTO memberTypeDTO) {
        this.memberTypeService.memberTypeUpdate(memberTypeDTO);
        return ResponseEntity.ok().build();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> memberTypeCreate(@Valid @RequestBody MemberTypeDTO memberTypeDTO) {
        this.memberTypeService.memberTypeCreate(memberTypeDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> memberTypeDelete(@PathVariable String id) {
        this.memberTypeService.memberTypeDelete(id);
        return ResponseEntity.ok().build();
    }
}
