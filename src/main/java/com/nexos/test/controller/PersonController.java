package com.nexos.test.controller;

import com.nexos.test.dto.PersonDTO;
import com.nexos.test.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
/**
 * @author Malejoab1990
 */
@RestController
@RequestMapping("/api/person")
@Validated
public class PersonController {
    private final PersonService personService;

    public PersonController(@Autowired PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonDTO> personByDocument(@NotBlank @RequestParam String document, @NotBlank @RequestParam String documentType) {
        return ResponseEntity.ok(personService.personByDocument(document, documentType));
    }

    @GetMapping(value = "/email/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonDTO> personByEmail(@PathVariable String email) {
        return ResponseEntity.ok(personService.personByEmail(email));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> personCreate(@RequestBody PersonDTO personDTO) {
        this.personService.personCreate(personDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> personUpdate(@RequestBody PersonDTO personDTO) {
        this.personService.personUpdate(personDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> personDelete(@PathVariable String id) {
        this.personService.personDelete(id);
        return ResponseEntity.ok().build();
    }
}