package com.nexos.test.controller;

import com.nexos.test.dto.FamilyDTO;
import com.nexos.test.service.FamilyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
/**
 * @author Malejoab1990
 */
@RestController
@RequestMapping("/api/family")
public class FamilyController {
    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FamilyDTO> findById(@PathVariable String id) {
        return ResponseEntity.ok(this.familyService.findById(id));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FamilyDTO>> findByName(@Valid @RequestParam @NotBlank String name) {
        return ResponseEntity.ok(this.familyService.findByName(name));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> familyCreate(@Valid @RequestBody FamilyDTO familyDTO) {
        this.familyService.familyCreate(familyDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> familyUpdate(@Valid @RequestBody FamilyDTO familyDTO) {
        this.familyService.familyCreate(familyDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<FamilyDTO> familyDelete(@PathVariable String id) {
        this.familyService.familyDelete(id);
        return ResponseEntity.ok().build();
    }
}