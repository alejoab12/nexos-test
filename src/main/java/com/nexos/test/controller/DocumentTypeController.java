package com.nexos.test.controller;

import com.nexos.test.dto.DocumentTypeDTO;
import com.nexos.test.service.DocumentTypeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
/**
 * @author Malejoab1990
 */
@RestController
@RequestMapping("/api/document-type")
public class DocumentTypeController {
    private DocumentTypeService documentTypeService;

    public DocumentTypeController(DocumentTypeService documentTypeService) {
        this.documentTypeService = documentTypeService;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DocumentTypeDTO>> documentTypeAll() {
        return ResponseEntity.ok(documentTypeService.documentTypeAll());

    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> documentTypeCreate(@Valid @RequestBody DocumentTypeDTO documentTypeDTO) {
        documentTypeService.documentTypeCreate(documentTypeDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> documentTypeUpdate(@Valid @RequestBody DocumentTypeDTO documentTypeDTO) {
        documentTypeService.documentTypeUpdate(documentTypeDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> documentTypeDelete(@Valid @PathVariable String id) {
        documentTypeService.documentTypeDelete(id);
        return ResponseEntity.ok().build();
    }
}
