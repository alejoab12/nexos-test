package com.nexos.test.audit;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nexos.test.constant.Constants;
import com.nexos.test.persistence.entity.AuditEntity;
import com.nexos.test.persistence.entity.AuditErrorEntity;
import com.nexos.test.service.AuditService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;
/**
 * @author Malejoab1990
 */
@Aspect
@Component
@Slf4j
public class AuditAspect {
    private final AuditService auditService;

    public AuditAspect(@Autowired AuditService auditService) {
        this.auditService = auditService;
    }

    @Around("@annotation(com.nexos.test.audit.Audit)")
    public Object transactionalLog(ProceedingJoinPoint joinPoint) throws Throwable {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Object objProccess = joinPoint.proceed();
        buildAudit(joinPoint.getArgs(), objProccess, joinPoint.getSignature().getName(), request.getRemoteAddr(), request.getHeader(Constants.USER_AGENT));
        return objProccess;
    }


    private void buildAudit(Object[] params, Object response, String method, String ip, String userAgent) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        auditService.auditCreate(AuditEntity.builder()
                .id(UUID.randomUUID().toString())
                .creationDate(new Timestamp(System.currentTimeMillis()))
                .method(method)
                .ip(Objects.isNull(ip) ? "" : ip)
                .userAgent(userAgent)
                .parameters(mapper.writeValueAsString(params)).response(mapper.writeValueAsString(response)).build());
    }


    @AfterThrowing(pointcut = "execution(* com.nexos.test.*.*.*(..))", throwing = "ex")
    public void logError(Exception ex) throws JsonProcessingException {

        this.auditService.auditErrorCreate(AuditErrorEntity.builder()
                .id(UUID.randomUUID().toString())
                .creationDate(new Timestamp(System.currentTimeMillis()))
                .message(ex.getMessage())
                .build());
    }


}

