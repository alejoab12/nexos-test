package com.nexos.test.audit;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
/**
 * @author Malejoab1990
 */
@Target({METHOD})
@Retention(RUNTIME)
@Documented
public @interface Audit {
}
