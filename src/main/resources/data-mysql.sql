INSERT IGNORE INTO `document_type` (`id`, `name`)
VALUES ('ad6e5a43-1396-4303-814d-9a74f8d753d6', 'Cedula'),
       ('ad6khf43-1391-4305-814d-9a52f8d753d6', 'TI'),
       ('ad6e5a43-1733-2335-814d-9a52f8d753d6', 'Pasaporte');
INSERT IGNORE INTO `member_type` (`id`, `name`)
VALUES ('ad6e5a43-1391-4205-816d-9a52f8d7a4u6', 'Padre'),
       ('ad6e5a43-2391-1108-874d-2a52f8d753d6', 'Madre'),
       ('ad9e1a42-1397-4305-863d-9a52f8d753d6', 'Hijo');
INSERT IGNORE INTO `person` (`id`, `document`, `document_type_id`, `name`, `last_name`, `email`, `phone`, `birthdate`)
VALUES ('ad6e5a43-1391-4205-816d-9a52f8d7a4u6', '1010194766', 'ad6e5a43-1396-4303-814d-9a74f8d753d6',
        'Manuel Alejandro', 'Alcala Bustos', 'alejoab12@hotmail.com', '3156566551', '1990-07-16');
INSERT IGNORE INTO `person` (`id`, `document`, `document_type_id`, `name`, `last_name`, `email`, `phone`, `birthdate`)
VALUES ('ad6e5a43-1391-4205-816d-9a52f8d7a4u6', '1010194766', 'ad6e5a43-1396-4303-814d-9a74f8d753d6',
        'Manuel Alejandro', 'Alcala Bustos', 'alejoab12@hotmail.com', '3156566551', '1990-07-16');
INSERT IGNORE INTO `person` (`id`, `document`, `document_type_id`, `name`, `last_name`, `email`, `phone`, `birthdate`)
VALUES ('ad6e5243-1394-4105-816d-9a12f1d7a4u6', '1010809234', 'ad6khf43-1391-4305-814d-9a52f8d753d6',
        'Martin Alejandro', 'Alcala Bustos', 'martin20@hotmail.com', '3156566551', '2015-02-08');

INSERT IGNORE INTO `family` (`id`, `name`) value ('ad9e1a42-1397-3305-263d-7a52f1d752d0', 'Alcala Bustos');

INSERT IGNORE INTO `person_family` (`id`, `person_id`, `family_id`, `member_type_id`)
values ('ae0e1a42-1397-3305-263d-2a12f1d652d0', 'ad6e5a43-1391-4205-816d-9a52f8d7a4u6',
        'ad9e1a42-1397-3305-263d-7a52f1d752d0', 'ad6e5a43-1391-4205-816d-9a52f8d7a4u6');

INSERT IGNORE INTO `person_family` (`id`, `person_id`, `family_id`, `member_type_id`)
values ('ae0e1a42-1397-3305-263d-2a12f1d653d1', 'ad6e5243-1394-4105-816d-9a12f1d7a4u6',
        'ad9e1a42-1397-3305-263d-7a52f1d752d0', 'ad9e1a42-1397-4305-863d-9a52f8d753d6');